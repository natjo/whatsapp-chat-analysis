#! /usr/bin/perl
use 5.26.1;
use warnings;
use strict;
use Data::Dumper;

# Transform a specified file in the whatsapp sent format to proper csv


if (scalar @ARGV < 2) {
    print "inputfile missing\nusage: $0 INPUT_FILE OUTPUT_FILE\n";
    exit;
}


my @allLines = readInAllLines($ARGV[0]);
say "Read in ".scalar @allLines." lines";


open OUTPUT_FILE, '>', $ARGV[1] || die "Could not open $ARGV[1]";
foreach my $line (@allLines){
    print OUTPUT_FILE transformLine($line)."\n";
}


sub readInAllLines{
    my ($inputFile) = @_;
    my @allLines;
    open INPUTFILE, $inputFile || die "Could not open $inputFile\n";;
    while(my $line = <INPUTFILE>){
        chomp $line;
        push @allLines, $line;
    }
    return @allLines;
}

sub transformLine{
    my ($line) = @_;
    $line =~ s/, /\t/;
    $line =~ s/ - /\t/;
    $line =~ s/: /\t/;
    return $line;
}
