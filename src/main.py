import pandas as pd
import argparse
import sys
import matplotlib.pyplot as plt
from datetime import datetime
import numpy as np
from scipy import signal
import my_excepthook


from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()


# Header: 'date-time', 'name', 'message'

# CONVOLUTION_VECTOR = [1,2,3,5,10,5,3,2,1]

# FILTER_WIDTH, FILTER_STD and MIN_DATE are only used for the graphs, not the bar plots
FILTER_WIDTH=10
FILTER_STD=3
MIN_DATE = datetime.strptime("01/01/2019", "%d/%m/%Y")

def main():
    args = get_cli_arguments()
    df = load_csv(args.file)

    # plot_msg_per_day(df)
    plot_chars_per_day(df)
    # plot_total_chars_per_name(df)
    # plot_total_msg_per_name(df)

    set_options_plots()
    plt.legend()
    plt.show()


def plot_total_chars_per_name(df):
    names = df['name'].unique()
    info = {}
    for name in names:
        msgs = df.loc[df['name'] == name]['message'].tolist()
        info[name] = sum([len(msg) for msg in msgs])

    plt.bar(range(len(info)), list(info.values()), align='center')
    plt.xticks(range(len(info)), list(info.keys()), rotation='vertical')


def plot_total_msg_per_name(df):
    names = df['name'].unique()
    info = {}
    for name in names:
        msgs = df.loc[df['name'] == name]['message'].tolist()
        info[name] = sum([1 for msg in msgs])

    plt.bar(range(len(info)), list(info.values()), align='center')
    plt.xticks(range(len(info)), list(info.keys()), rotation='vertical')


def plot_chars_per_day(df):
    print('Plot {}'.format(', '.join(df['name'].unique())))
    for name in df['name'].unique():
        plot_chars_per_day_single(df, names=[name])


def plot_chars_per_day_single(df, names):
    df_name = df.loc[df['name'].isin(names)]
    dates = df_name['date'].unique()
    chars_per_day = {datetime.strptime(date_str, "%d/%m/%Y"):get_chars_of_day(df_name, date_str) for date_str in dates}
    # Filter old dates, which no information
    chars_per_day = {date: val for date, val in chars_per_day.items() if date > MIN_DATE }

    plot_on_dates(chars_per_day, ', '.join(names))


def get_chars_of_day(df, date_str):
    msgs = df.loc[df['date'] == date_str]['message'].tolist()
    return sum([len(msg) for msg in msgs])


def plot_msg_per_day(df):
    print('Plot {}'.format(', '.join(df['name'].unique())))
    for name in df['name'].unique():
        plot_msg_per_day_single(df, names=[name])


def plot_msg_per_day_single(df, names):
    df_name = df.loc[df['name'].isin(names)]
    msg_per_day = df_name['date'].value_counts().to_dict()
    msg_per_day = {datetime.strptime(date, "%d/%m/%Y"): val for date, val in msg_per_day}
    # Filter old dates, which no information
    msg_per_day = {date: val for date, val in msg_per_day.items() if date > MIN_DATE }

    plot_on_dates(msg_per_day, ', '.join(names))


# Expects a dictionary with {date_object: count} for info parameter
def plot_on_dates(info, label):
    # for key, value in sorted(msg_per_day.items()):
    #     print("{} : {}".format(key, value))

    x,y = zip(*sorted(info.items()))

    window = signal.gaussian(FILTER_WIDTH, std=FILTER_STD)
    window = window/np.sum(window)
    print('Filter used for {}: {}'.format(label, window))
    y = np.convolve(y, window, mode='same')

    plt.plot(x,y, label=label)


def load_csv(csv_file):
    # lines = csv_file.readlines()
    df = pd.read_csv(csv_file, sep='\t', names=['date', 'time', 'name', 'message'])
    # Some lines have wrong values, due to line breaks
    clean_df = df.dropna(axis=0)
    return clean_df


def get_cli_arguments():
    parser = argparse.ArgumentParser(description='Create some pretty plots from whatsapp chats transformed to csv')
    parser.add_argument('-f', '--file',
                       type=argparse.FileType('r'),
                       help='File to read the chat from.')
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    return parser.parse_args()


def set_options_plots():
    ax = plt.subplot(111)
    ax.spines["top"].set_visible(False)
    ax.spines["right"].set_visible(False)

    plt.ylabel('val')
    plt.xlabel('date')
    plt.grid(True)

if __name__ == '__main__':
    main()



