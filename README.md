# How to use
1. Get chats as text file from whatsapp (you can find it in the settings of the app)
2. Save it in the `data` folder
3. Call the `transform2csv.pl` script to transform the txt file to a csv file:
```
./transform2csv.pl data/messages.txt data/messages.csv
```
4. Check the main function in the `src/main.py` file to set the right plots you want to have
5. Run `src/main.py -f data/messages.csv` to create a plot
